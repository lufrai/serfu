# serfu

## 0.2.0

### Minor Changes

- a460b8b: Updated all dependencies. The `@solidjs/start` peerDependency now must be v1.1 or higher.

## 0.1.1

### Patch Changes

- e6dc60a: Added ts function overloads to `useServerRef`. The `initialValue` argument is now optional.

## 0.1.0

### Minor Changes

- c976fec: Updated the dependencies.

## 0.0.2

### Patch Changes

- 042154d: Implemented the `useServerRef` utility.
