import { getServerFunctionMeta } from "@solidjs/start";
import { getRequestEvent } from "solid-js/web";

const STORES = "serverRefs$$";
const INDEX = "serverRefIdx$$";

const stores: Record<string, Record<number, any>> = ((globalThis as any)[
  STORES
] ??= {});

export function useServerRef<T>(): { current?: T };
export function useServerRef<T>(initialValue: T): { current: T };
export function useServerRef<T>(initialValue?: T): { current?: T } {
  const { id } = getServerFunctionMeta()!;
  const store = (stores[id] ??= {});

  const request = getRequestEvent()!;
  request.locals[INDEX] ??= -1;
  const refIdx = ++request.locals[INDEX];

  store[refIdx] ??= { current: initialValue };
  return store[refIdx];
}
