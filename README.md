# Serfu

A library of tiny server-side utilities for [SolidStart](https://start.solidjs.com/).

## Installation

Run: `pnpm install -D serfu`

> Depending on your project structure, you might have to add serfu to `ssr.noExternal` in `app.config.ts`:
> ```ts
> export default defineConfig({
>   vite: {
>     ssr: {
>       noExternal: ["serfu"],
>     },
>   },
> });
> ```

## Utilities

### useServerRef

You need **local** memory in your server functions that is stable across the ssr and server-functions Vinxi routers? `useServerRef` can be used inside server functions to create and access local memory. This follows similar rules like React Hooks:
- You can call it multiple times, every call returns a different memory location
- You should not change the order of the calls at runtime
- You should not call useServerRef in a condition

#### Usage

```tsx
import { useServerRef } from "serfu";

const count = async () => {
  "use server"
  const value = useServerRef(0);
  value.current++;
  return value.current;
};
```

#### Use Cases

- ✅ Store **local**, unnamed state related to a server function
- ✅ Memoize expensive server function logic
- ✅ Cache server function results
- ⛔️ Store **global**, named state, that can be accessed from multiple places on the server
  - Use [Unstorage](https://github.com/unjs/unstorage) or a DB of your choice instead